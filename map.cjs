function functionMap(items, callbackFunc) {
  let resultArray = [];
  if (!Array.isArray(items) || items.length === 0) {
    return [];
  } else {
    for (let index = 0; index < items.length; index++) {
      resultArray.push(callbackFunc(items[index], index, items));
    }
  }
  return resultArray;
}

module.exports = functionMap;
