function each(items, callbackFunc) {
  let resultArray = [];
  if (items.length == 0) {
    return [];
  }
  if (Array.isArray(items) === false) {
    return undefined;
  } else {
    for (let index = 0; index < items.length; index++) {
      callbackFunc(items[index], index);
    }
  }
}
// function callbackFunc(ele, index) {   // we can't write here callback function.
//   return ele + index;
// }

module.exports = each;
