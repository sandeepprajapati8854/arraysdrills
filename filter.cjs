function functionFilter(dataArr, callbackFunc) {
  let answerArr = [];
  if (Array.isArray(dataArr) == false) {
    return [];
  }
  if (dataArr.length == 0) {
    return [];
  } else {
    for (let index = 0; index < dataArr.length; index++) {
      let recieve = callbackFunc(dataArr[index], index, dataArr);
      if (recieve == true) {
        answerArr.push(dataArr[index]);
      }
    }
  }
  return answerArr;
}

module.exports = functionFilter;
