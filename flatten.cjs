let flatArray = [];
function functionFlatten(array, depth = 1) {
  if (!Array.isArray(array) || array.length === 0) {
    return [];
  }
  for (let index = 0; index < array.length; index++) {
    // console.log("Element is " + array[index]);
    if (Array.isArray(array[index]) && depth >= 1) {
      functionFlatten(array[index], depth - 1);
      // console.log(flatArray);
    } else if (Array.isArray(array[index]) && depth < 1) {
      flatArray.push(array[index]);
      // console.log(flatArray);
    } else {
      if (array[index] != undefined) {
        flatArray.push(array[index]);
        //   console.log(flatArray);
      }
    }
  }
  return flatArray;
}
module.exports = functionFlatten;
