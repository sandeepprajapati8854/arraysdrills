const functionMap = require("../map.cjs");
let items = [1, 2, 3, 4, 5, 5];

function callbackFunc(ele, index, items) {
  return ele * 10;
}

let result = functionMap(items, callbackFunc);
console.log(result);
