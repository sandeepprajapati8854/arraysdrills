const each = require("../each.cjs");
const items = [1, 2, 3, 4, 5, 5];

function callbackFunc(ele, index) {
  console.log(ele);
}
each(items, callbackFunc);
