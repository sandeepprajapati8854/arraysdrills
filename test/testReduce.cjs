const functionReduce = require("../reduce.cjs");

const items = [1, 2, 3, 4, 5, 5];
let startingValue;

function callbackFunc(accumulator, currentValue, index, items) {
  return accumulator + currentValue;
}

let result = functionReduce(items, callbackFunc, startingValue);
console.log(result);
