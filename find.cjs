function functionFind(items, callbackFunc) {
  let answer;
  if (Array.isArray(items) == false) {
    return 0;
  }
  if (items.length == 0) {
    return 0;
  } else {
    for (let index = 0; index < items.length; index++) {
      let recieve = callbackFunc(items[index]);
      if (recieve > 10) {
        answer = recieve;
        break;
      }
    }
    return answer;
  }
}
module.exports = functionFind;
