function functionReduce(items, callbackFunc, startingValue) {
  if (Array.isArray(items) == false) {
    return 0;
  } else {
    if (items.length == 0 && startingValue == undefined) {
      return undefined;
    }
    if (items.length == 0 && startingValue != undefined) {
      return startingValue;
    }
    // this concept based on Questions
    if (startingValue == undefined && items.length != 0) {
      startingValue = items[0];
      let index = 1;
      for (index = 1; index < items.length; index++) {
        startingValue = callbackFunc(startingValue, items[index], index, items);
      }
      // this is behaviour of reduce.
    } else {
      for (index = 0; index < items.length; index++) {
        startingValue = callbackFunc(startingValue, items[index], index, items);
      }
    }
    return startingValue;
  }
}

//   let resultArray = [];
// let answer;
// if (items.length == 0) {
//   return startingValue;
// }
// if (startingValue == undefined) {
//   return items[0];
// }
// if (Array.isArray(items) === false) {
//   return undefined;
// } else {
//   for (let index = 0; index < items.length; index++) {
//     answer = callbackFunc(startingValue, items[index]);
//     startingValue = answer;
//   }
// }

module.exports = functionReduce;
